import { BrowserRouter, Routes, Route } from "react-router-dom";

import { StarsWithPlanet } from "./elements/Page_1/StarsWithPlanet";
import { BigPlanetMenu } from "./elements/Page_2/BigPlanetMenu";

function App() {
  const rotate = new Date().getMinutes() % 2 !== 0;
  return (
    // Рендер елементів першої та другої сторінки та їх роутінг(можна було робити без
    //   задіювання роутінгу і другої сторінки,але цікаво було спробувати цей елемент React)
    <BrowserRouter>
      <Routes>
        <Route path="BigPlanetMenu" element={<BigPlanetMenu />} />
        <Route path="/" element={<StarsWithPlanet rotate={rotate} />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
