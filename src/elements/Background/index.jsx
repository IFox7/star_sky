import React, { Component } from "react";
import { CSSTransition } from "react-transition-group";

import "./style.css";

import "./images/giphy 1.png";
import "./images/Vector (1).png";
import "./images/rocket.png";

import Menu from "../Menu";

// Клас для відображення 2,3 сторінки проекту
class Background extends Component {
  //  Стейт для запуску анімації планети та появи і руху тексту з картинкою ракети
  state = {
    counter: new Date().getSeconds(),
    activeClass: false,
    movePlanet: false,
  };
  // Функція запуску анімації стрибаючої стрілки низу екрана
  handleAnimationEnd = () => {
    const node = document.querySelector(".arrow_down");
    if (this.state.activeClass === true) {
      node.classList.remove("animate__fadeIn");
      node.classList.add("animate__shakeY");
    }
  };
  // Зміна стейта для запуску стрибків стрілки
  componentDidMount() {
    this.counterStart = setInterval(
      () =>
        this.setState((state) => {
          return {
            ...state,
            counter: new Date().getSeconds(),
            activeClass: true,
          };
        }),
      4000
    );
  }
  componentWillUnmount() {
    clearInterval(this.counterStart);
  }
  // Функція запуску зміни планети і заголовка на фото ракети і текст
  arrowClick = () => {
    this.setState((state) => {
      return {
        ...state,
        movePlanet: true,
      };
    });
    const rocket = document.getElementById("rocket"),
      rocketText = document.getElementById("rocketTextWrapper"),
      arrow_down = document.querySelector(".arrow_down");
    rocket.classList.remove("visibilityRocket");
    rocketText.classList.remove("visibilityRocket");
    arrow_down.classList.add("visibilityRocket");
  };
  // Рендер елемента
  render() {
    this.handleAnimationEnd();
    return (
      <>
        <div className="stars"></div>
        <div className="flicker">
          <Menu />
          <div className="content_wrapper">
            {/* Початковий заголовок */}
            <CSSTransition
              in={this.state.movePlanet}
              timeout={300}
              classNames="titleMoving"
            >
              <div className="bigTitle animate__animated animate__fadeIn animate__delay-5s animate__faster 5s">
                Un viaje al espacio
              </div>
            </CSSTransition>

            {/* Зображення ракети */}
            <CSSTransition
              in={this.state.movePlanet}
              timeout={500}
              classNames="rocketImage"
            >
              <img
                className="rocket visibilityRocket"
                id="rocket"
                src="./images/rocket.png"
                alt="rocket"
              />
            </CSSTransition>
            {/* Текст справа від ракети */}
            <CSSTransition
              in={this.state.movePlanet}
              timeout={500}
              classNames="rocketText"
            >
              <div
                className="rocketTextWrapper visibilityRocket "
                id="rocketTextWrapper"
              >
                <div className="rocketTextTitle">Titulo</div>
                <div className="rocketTextText">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Semper viverra tempor, enim vulputate nunc interdum sit diam
                  ultrices. Sed erat volutpat curabitur ornare in facilisi
                  ornare. Vitae mollis sed feugiat ipsum condimentum eget magnis
                  nulla at. Massa semper massa quisque tincidunt cursus.
                  Elementum aliquet sed lectus facilisis massa in. Felis lectus
                  egestas urna egestas arcu. Quam quisque volutpat lacus, eget.
                  Quis risus, rhoncus nisi a, sit libero ut viverra. Magna quis
                  hendrerit in cursus. Sed sed vitae ullamcorper dignissim
                  tristique. Imperdiet vulputate blandit eu egestas massa a
                  mauris libero. Mi turpis sagittis ac elit id sollicitudin
                  urna. Velit neque neque vitae ultrices sagittis hendrerit in
                  cursus. Sed egestas commodo mi sed. Aliquam at nunc,
                  vestibulum viverra ipsum. Libero scelerisque tortor
                  pellentesque ante ut sit nunc, vitae. ulla donec ultrices quis
                  eu adipiscing habitant.
                </div>
              </div>
            </CSSTransition>

            {/* Велике зображення планети */}
            <CSSTransition
              in={this.state.movePlanet}
              timeout={300}
              classNames="planetMoving"
            >
              <img
                className="bigPlanet"
                id="bigPlanet"
                src="./images/giphy 1.png"
                alt="planet"
              />
            </CSSTransition>
            {/* Стрілка,що стрибає після появи */}
            <img
              src="./images/Vector (1).png"
              onClick={this.arrowClick}
              className="arrow_down animate__animated animate__fadeIn animate__infinite animate__delay-5s animate__slower 5s"
              alt="arrow_down"
            />
          </div>
        </div>
      </>
    );
  }
}

export default Background;
