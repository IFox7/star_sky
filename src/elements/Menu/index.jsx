import React from "react";

import "./style.css";

// Елемент меню з класами анімації
function Menu() {
  return (
    <>
      <nav className="menu_wrapper">
        <ul>
          <li className="animate__animated animate__bounceInLeft">Inicio</li>
          <li className="animate__animated animate__bounceInDown animate__delay-2s">
            Espacio
          </li>
          <li className="animate__animated animate__bounceInUp animate__delay-3s">
            Planetas
          </li>
          <li className="animate__animated animate__bounceInDown animate__delay-4s">
            Nosotros
          </li>
          <li className="animate__animated animate__bounceInRight animate__delay-1s">
            Misiones
          </li>
        </ul>
      </nav>
    </>
  );
}

export default Menu;
