import React, { Component } from "react";

import { CSSTransition } from "react-transition-group";

import "animate.css";
import "./style.css";

import "./images/giphy 1.png";

// Елемент першої сторінки з планетою,що обертається в різні сторони, в залежності від
// парної чи непарної хвилини
export class StarsWithPlanet extends Component {
  // Стейт для запуска анімації
  state = {
    // rotate: new Date().getMinutes() % 2 !== 0,
    rotate: this.props.rotate,
  };

  // Функція зміни напрямка руху планети
  changeRotary = () => {
    if (new Date().getMinutes() % 2 === 0) {
      this.setState((state) => {
        return {
          ...state,
          rotate: true,
        };
      });
    } else if (new Date().getMinutes() % 2 !== 0) {
      this.setState((state) => {
        return {
          ...state,
          rotate: false,
        };
      });
    }
  };
  // Рендер елемента
  render() {
    return (
      <>
        <div className="stars"></div>
        <div className="flicker">
          {/* Посилання на нову сторінку з великим зображенням планети і стрибайучою стрілкою */}
          <a href="/BigPlanetMenu">
            {/* Зображення анімованої планети */}
            <CSSTransition
              in={this.state.rotate}
              timeout={300}
              classNames="moving"
              onEnter={this.changeRotary()}
              onExited={this.changeRotary()}
            >
              <img
                className="planet"
                id="planet"
                src="./images/giphy 1.png"
                alt="planet"
              />
            </CSSTransition>
          </a>
        </div>
      </>
    );
  }
}
