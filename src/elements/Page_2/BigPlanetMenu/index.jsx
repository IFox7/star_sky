import React from "react";
import Background from "../../Background";

export function BigPlanetMenu() {
  return (
    <>
      <div className="wrapper_bigPlanet">
        <Background />
      </div>
    </>
  );
}
