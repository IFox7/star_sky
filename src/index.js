import React from "react";
import ReactDOM from "react-dom/client";

import App from "./App";

// Головна сторінка
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<App />);
